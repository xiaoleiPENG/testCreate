布局
	.container (固定宽度)
	.container-fluid (全屏宽度) 



文字排版

	标签：
		h1-6  2.5rem = 40px - 1rem = 16px
		small 副标题
		mark  高亮
		abbr  虚线
		code  代码
		kbd   黑框
		pre   小文字段


	类：
		.display-1 - 4  大到小
		.text-left	左对齐	尝试一下
		.text-center	居中	尝试一下
		.text-right	右对齐
		.pre-scrollable   滚动条


表格
	
	类：
		.table table-striped 条纹表格
		.table table-bordered 带边框的表格
		.table table-hover  鼠标移入变色
		.table table-bordered table-sm 小表格
		
		
图像形状
	类
		.rounded 圆角
		.rounded-circle 椭圆
		.img-thumbnail 边框
		.img-fluid 自适应大小


按钮
	类：
		.btn btn-*  按钮
		.btn-lg 大按钮
		.btn-sm 小按钮
		.active 激活
		.disabled 禁用
		
		.btn-group 
			.btn btn-primary
			.btn btn-primary
			.btn btn-primary 按钮组
			
		.btn-group-vertical 垂直按钮组

		.btn btn-primary dropdown-toggle dropdown-toggle-split data-toggle="dropdown"
			.dropdown-menu
				a .dropdown-item 下拉菜单
				
				
进度条
	类：
		.progress
			.progress-bar style="width:70%"  进度条	
			.progress-bar progress-bar-striped  条纹进度条
			.progress-bar progress-bar-striped progress-bar-animated 条纹度化进度条
			.progress-bar bg-*  进度条颜色
			
			
分页
	类：
		.pagination
			.page-item 
				a.page-link 普通分页
			.page-item active 页数高亮
			.page-item disabled 不可点击
			
		.breadcrumb	
			.breadcrumb-item
				a    面包屑导航
				
				
提示
	类：
		data-toggle="tooltip" title="我是提示内容!"
		data-toggle="tooltip" data-placement="top  bottom   left  right "  上下左右显示
		
		
			